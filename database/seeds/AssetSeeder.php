<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assets')->insert([
        	'name' => 'Asset 1',
            'control_code' => 'PLDT',
            'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ratione ea dignissimos odio dicta omnis voluptatibus quo delectus sit adipisci.",
            'image' => "https://via.placeholder.com/150",
            'category_id' => 1,
            'status_id' => 1
        ]);
        
    }
}
