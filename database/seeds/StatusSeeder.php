<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('statuses')->insert([
        	'name' => 'Pending'
        ]);

        DB::table('statuses')->insert([
        	'name' => 'Approved'
        ]);

        DB::table('statuses')->insert([
        	'name' => 'Denied'
        ]);

        DB::table('statuses')->insert([
            'name' => 'Completed'
        ]);

    }
}
