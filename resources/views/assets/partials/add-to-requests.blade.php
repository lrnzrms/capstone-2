<form action="{{ route('requests.update', $asset->id) }}" method="post" class="my-2">
	@csrf
	@method('PUT')
	<input type="number" name="quantity" id="" class="form-control form-control-sm w-50" min="1" value="1">
	<button class="btn btn-sm btn-success w-100 mt-1">Request</button>
</form>