<form action="{{route('requests.update', $asset->id)}}" method="post" class="mb-1">
	@csrf
	@method('PUT')
	<button class="btn btn-sm btn-success w-100 mt-1"><i class="fas fa-plus-square fa-lg"></i> Request</button>
</form>
