<form action="{{ route('assets.destroy', $asset->id) }}" method="post">
	@csrf
	@method('DELETE')
	<button class="btn btn-sm btn-danger w-100"><i class="far fa-trash-alt fa-lg"></i> Delete</button>
</form>
