@extends('layouts.app')

@section('content')
	
	<div class="container">
		
		@includeWhen(Session::has('message'), 'partials.alert')

		<div class="row">
			<div class="col-12 col-md-4">
				<img src="{{ $asset->image }}" alt="" class="w-100">
			</div>	
			<div class="col-12 col-md-8">
				<h1>{{ $asset->name }}</h1>
				{{-- description start --}}
				<div class="row">
					<div class="col-2">Description:</div>
					<div class="col">{{ $asset->description }}</div>
				</div>
				{{-- description end --}}

				{{-- category start --}}
				<div class="row">
					<div class="col-2">Category:</div>
					<div class="col">
						<span class="badge badge-info">{{ $asset->category->name }}
						</span>
					</div>
				</div>
				{{-- category end --}}

				{{-- status start --}}
				<div class="row">
					<div class="col-2">Status:</div>
					<div class="col">
						<span class="badge badge-@if($asset->status_id === 1)
							badge-success
							@elseif($asset->status_id === 2)
							badge-danger
							@elseif($asset->status_id === 3)
							badge-warning
							@endif
							">{{ $asset->asset_status->name }}</span>
						</div>
					</div>
				{{-- status end --}}

				@cannot('isAdmin')
					@include('assets.partials.add-to-requests')
				@endcannot
				@cannot('isAdmin')
				@include('assets.partials.request-btn')
				@endcannot

				@can('isAdmin')
					{{-- admin btns start --}}
					<div class="row">
						{{-- edit start --}}
						<div class="col col-md-4">
							<a href="{{ route('assets.edit', $asset->id) }}" class="btn btn-sm btn-warning w-100 mb-1">Edit</a>
						</div>
						{{-- edit end --}}

						{{-- delete start --}}
						<div class="col col-md-4">
							<form action="{{ route('assets.destroy', $asset->id) }}" method="post">
								@csrf
								@method('DELETE')

								<button class="btn btn-sm btn-danger w-100 mb-1">Delete</button>
							</form>
						</div>
						{{-- delete end --}}
					</div>
					{{-- admin btns end --}}
				@endcan
			</div>
		</div>
	</div>
@endsection

