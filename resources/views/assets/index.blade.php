@extends('layouts.app')

@section('content')

<div class="container-fluid">
	@includeWhen(Session::has('message'), 'partials.alert')
	<div class="row">
		<div class="col-xl-10 col-lg-9 col-md-8 ml-auto p-4">
			{{-- header --}}
			<div class="row">
				<div class="col-12">
					<h1 class="text-center">
						Assets
					</h1>
				</div>
				@if(count($assets) === 0)
				<div class="col-8 mt-4 mx-auto">
					@include('partials.no-entry-alert', [
						'title' => 'assets'
						])
					</div>
					@endif
				</div>
				{{-- end header --}}

				{{-- unit list --}}
				<div class="row mt-5">
					<div class="col-12 mx-auto">
						<div class="row">
							@foreach($assets as $asset)
							<div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2">
								{{-- unit card --}}
								<div class="card">
									<img src="{{ $asset->image }}" alt="" class="card-img-top">
									<div class="card-body">
										<div class="card-body">
											<h3 class="card-title">
												{{ $asset->name }}
											</h3>
											<p class="card-title">
												Control Code: {{$asset->id}}-{{ $asset->control_code }}
											</p>

											<h5>Category:
												<span class="badge badge-primary ">{{$asset->category->name}}</span>
											</h5>

											<h4 class="card-text mb-0">
												Status:	<span
												class="badge
												@if($asset->status_id === 1)
												badge-success
												@elseif($asset->status_id === 2)
												badge-danger
												@elseif($asset->status_id === 3)
												badge-warning
												@endif
												">{{ $asset->asset_status->name }}</span>
											</h4>
										</div>
										<div class="card-footer bg-light">
											@include('assets.partials.request-btn')
											<a href="{{ route('assets.show', $asset->id) }}" class="btn btn-sm btn-info w-100 mb-1">View</a>

											@can('isAdmin')
											@include('assets.partials.edit-btn')
											@include('assets.partials.delete-form')
											@endcan
										</div>
									</div>
									{{-- unit card end --}}
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
				{{-- end unit list --}}
			</div>
		</div>
	</div>

	@endsection
