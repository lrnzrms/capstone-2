@extends('layouts.app')

@section('content')
		<div class="card-body">
			<div class="row">
				<div class="col-12 col-sm-6 mx-auto">
					<form action="{{route('assets.store')}}" method="post" enctype="multipart/form-data">
						@csrf
					{{-- name --}}
						@include('assets.partials.form-group',[
							'name' => 'name',
							'type' => 'text',
							'classes' => ['form-control', 'form-control-sm']
						])

						{{-- price --}}
						@include('assets.partials.form-group',[
							'name' => 'price',
							'type' => 'text',
							'classes' => ['form-control', 'form-control-sm']
						])

						{{-- image --}}
						@include('assets.partials.form-group',[
							'name' => 'image',
							'type' => 'file',
							'classes' => ['form-control-file', 'form-control-sm']
						])
						
						{{-- category_id --}}
						<label for="category_id">Asset category:</label>
						<select name="category_id" id="category_id" class="form-control form-control-sm @error('category_id')
									is-invalid
								@enderror">
							<option>Select an Option</option>
							{{-- @foreach($categories as $category)
								<option value={{ $category->id }}>{{ $category->name }}</option>
							@endforeach	 --}}
						</select>
						@error('category_id')
						@if('category_id' != $category->name)
								<small class="d-block invalid-feedback">
									<strong>					
										Please select category
									</strong>
								</small>
						@endif
						@enderror

						
						{{-- description --}}
						<label for="description">Description:</label>
						<textarea 
							name="description" 
							id="description" 
							class="form-control form-control-sm
								@error('description')
									is-invalid
								@enderror

							" 
							cols="30" 
							rows="10"></textarea> 
						@error('description')
							<small class="d-block invalid-feedback">
								<strong>					
									{{ $message }}
								</strong>
							</small>
						@enderror	

						<div class="form-group">
							<button class="btn btn-sm btn-primary mt-1">Add Asset</button>
						</div>
					</form>
				</div>	
			</div>
		</div>
	</div>
@endsection