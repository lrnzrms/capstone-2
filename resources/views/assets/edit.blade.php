@extends('layouts.app')
@section('content')

	<div class="container-fluid custom-container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-8 col-lg-6">
				<h1 class="text-center">Edit Asset</h1>
			</div>
		</div>

		{{-- form start --}}
		<div class="row justify-content-center">
			<div class="col-12 col-md-8 col-lg-6">
				<form
					action="{{ route('assets.update', $asset->id)}}"
					method="post"
					enctype="multipart/form-data"
				>
					@csrf
					@method('PUT')
					{{-- name --}}
					<div class="form-group">
						<label for="name">Asset name</label>
						<input 
							type="text" 
							name="name" 
							id="name" 
							class="form-control 
										@error('name')
											is-invalid
										@enderror
										form-control-sm"
							value="{{ $asset->name }}" autofocus>
							@error('name')
								<small class="d-block invalid-feedback">
									<strong>{{ $message }}</strong>
								</small> 
							@enderror
						</div>

                    {{-- control code --}}
                    <div class="form-group">
                        <label for="control_code_show">Control code</label>
                        <input type="text" name="control_code_show" id="control_code_show" class="form-control form-control-sm" value="{{$asset->control_code}}" disabled>
                        <input type="hidden" name="control_code" id="control_code" value="{{$asset->control_code}}">
                    </div>


					{{-- image --}}
                   @include('assets.partials.form-group',[
                   	'name' => 'image',
                   	'type' => 'file',
                   	'value' => $asset->image,
                   	'classes' => ['form-control-file', 'form-control-sm']
                   ])

                   {{-- category_id --}}
						<label for="category_id">Asset category:</label>
						<select name="category_id" id="category_id" class="form-control">
							@foreach($categories as $category)
								<option value="{{ $category->id }}"{{ $category->id === $asset->category_id ? "selected" : ""}}>{{ $category->name }}</option>
							@endforeach	
						</select>
						
						{{-- description --}}
						<label for="description">Description:</label>
						<textarea 
							name="description" 
							id="description" 
							class="form-control form-control-sm
								@error('description')
									is-invalid
								@enderror

							" 
							cols="30" 
							rows="10">{{ $asset->description }}</textarea> 
						@error('description')
							<small class="d-block invalid-feedback">
								<strong>		
									{{ $message }}
								</strong>
							</small>
						@enderror

                    <label for="status_id">Status:</label>
					<select name="status_id" id="status_id" class="form-control form-control-sm">
						@foreach($asset_statuses as $asset_status)
							<option
								value="{{$asset_status->id}}"
								{{ $asset_status->id === $asset->status_id ? "selected" : ""}}
							>
								{{$asset_status->name}}
							</option>
						@endforeach
                    </select>
                    @error('status_id')
						<small class="d-block invalid-feedback">
							<strong>
								{{ $message }}
							</strong>
						</small>
                    @enderror

					<button class="btn btn-md btn-warning my-2">Edit Unit</button>

				</form>
			</div>
		</div>
		{{-- form end --}}
	</div>

@endsection
