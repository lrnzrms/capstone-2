{{-- @extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Create New Category
				</h1>
			</div>
		</div>
		
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		start form section
		<div class="row">
			<div class="col-12 col-sm-6 mx-auto">
				<form action="{{ route('categories.store') }}" method="post" enctype="multipart/form-data">
					@csrf
					
					<label for="name">Category name:</label>
					<input type="text" name="name" id="name" class="form-control form-control-sm">
					<button class="btn btn-sm btn-primary mt-1">Create Category</button>
				</form>
			</div>
		</div>
		{{-- end form section --}}
	{{-- </div>
@endsection() --}} 

@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-10 col-lg-9 col-md-8 ml-auto p-4">
            {{-- header --}}
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="text-center lead display-4">
                        Create Item
                    </h1>
                </div>
            </div>
            {{-- end header --}}

            {{-- Create item --}}
            <div class="row mt-5">
                <div class="col-12 col-md-6 mx-auto">
                    {{-- form --}}
                    <form action="{{ route('categories.store') }}" method="post">
                        @csrf
                        <label for="name">Category name:</label>
                        <input type="text" name="name" id="name"
                            class="
                                form-control
                                form-control-sm
                                @error('name')
									is-invalid
								@enderror
                            "
                        required>
                        @error('name')
                            <small class="d-block invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </small>
                        @enderror
                        <button class="btn btn-md btn-primary mt-2">
                            Create Item
                        </button>
                    </form>
                    {{-- end form --}}
                </div>

            </div>
            {{-- end Create item --}}
        </div>
    </div>
</div>
@endsection
