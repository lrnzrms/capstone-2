<div class="card mb-2">
	<div class="card-body">
		<h4 class="card-title text-center">
			{{ $category->name }}
		</h4>
		<h3>
			<span class="badge"></span>
		</h3>
		<p>Current Available: {{ $category->available_stocks }}/{{ $category->total_stocks }}</p>
	</div>
	<div class="card-footer text-center">
		@if(!isset($view))
		<a href="{{ route('categories.show', $category->id) }}" class="btn btn-success btn-sm mx-2">View</a>
		@endif
		@can('isAdmin')
		<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning btn-sm mx-2">Edit</a>
		<form action="{{ route('categories.destroy', $category->id) }}" method="post">
			@csrf
			@method('DELETE')
			<button class="btn btn-danger btn-sm">Delete</button>
		</form>
		@endcan
	</div>
</div>