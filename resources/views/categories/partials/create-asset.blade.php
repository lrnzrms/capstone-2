<div class="row">
    <div class="col-12">
        <h4 class="text-center lead my-4">
            Create {{$category->name}} Unit
        </h4>
    </div>
</div>

{{-- unit section --}}
<div class="row">
    <div class="col-12">
        {{-- add unit form --}}
        <form action="{{ route('assets.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            {{-- name --}}
            @include('assets.partials.form-group',[
                            'name' => 'name',
                            'type' => 'text',
                            'classes' => ['form-control', 'form-control-sm']
                        ])
            {{-- control code--}}
            <div class="form-group">
                <label for="control_code">Control Code</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-control-code">{{$category->id}}-</span>
                    </div>
                    <input type="text" name="control_code" id="control_code"
                        class="
                            form-control
                            @error('control_code')
									is-invalid
							@enderror
                        "
                        placeholder="Control-Code" aria-label="Control-Code"
                        aria-describedby="basic-control-code" required
                    >
                    @error('control_code')
                        <small class="d-block invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </small>
                    @enderror
                </div>
            </div>


            {{-- category model --}}
            <div class="form-group">
                <label for="item_model">Category Model</label>
                <select id="item_model" name="item_model" class="form-control" disabled>
                    <option value="{{$category->id}}">{{$category->name}}</option>
                </select>
                <input type="hidden" name="category_id" id="category_id" value="{{$category->id}}">
            </div>

            {{-- image --}}
            @include('assets.partials.form-group',[
                'name' => 'image',
                'type' => 'file',
                'classes' => ['form-control-file', 'form-control-sm']
                ])

            {{-- description --}}
            <label for="description">Description:</label>
            <textarea 
            name="description" 
            id="description" 
            class="form-control form-control-sm
            @error('description')
            is-invalid
            @enderror

            " 
            cols="8" 
            rows="3"></textarea> 
            @error('description')
            <small class="d-block invalid-feedback">
                <strong>                    
                    {{ $message }}
                </strong>
            </small>
            @enderror   

            <button class="btn btn-block btn-primary my-3"><i class="fas fa-plus fa-lg"></i> Add Asset</button>

        </form>
        {{-- add unit form end --}}
    </div>
</div>
{{-- add unit section end --}}
