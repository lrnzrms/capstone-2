@extends('layouts.app')

@section('content')
	{{-- alert message --}}
	@includeWhen(Session::has('message'), 'partials.alert')
	<div class="card-header mx-4">
			<h1>Categories</h1>
		</div>
	<div class="row mx-4">
		{{-- card start --}}
		@foreach($categories as $category)
			<div class="col-12 col-sm-6 col-md-4 col-lg-3 ">
				
				@include('categories.partials.card')

			</div>
		@endforeach
	</div>

@endsection


