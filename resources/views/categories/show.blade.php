@extends('layouts.app')

@section('content')
<div class="container">
    {{-- alert message --}}
    @includeWhen(Session::has('message'), 'partials.alert')
    {{-- end of alert message --}}

    <div class="row">
        {{-- item details and create item unit --}}
        <div class="col-12 col-md-4 col-lg-3 mb-5 bg-light">
            <div class="bottom-border pb-3">
                <h1 class="display-lg-4">{{$category->name}}</h1>
                <p class="lead mb-2">Stocks Available: {{$category->available_stocks}} / {{$category->total_stocks}}</p>
                @can('isAdmin')
                    <a href="{{route('categories.edit', $category->id)}}" class=" btn btn-sm btn-warning"><i class="far fa-edit fa-lg"></i> Edit</a>
                    <form action="{{ route('categories.destroy', $category->id) }}" method="post">
                    	@csrf
                    	@method('DELETE')
                    	<button class="btn btn-danger btn-sm">Delete</button>
                    </form>
                @endcan
                <hr>
            </div>
            
            @can('isAdmin')
                @include('categories.partials.create-asset')
            @endcan
        </div>
		
        {{-- end of category details and create category unit --}}
        <div class="col-12 col-md-8 col-lg-9">
            <div class="row">
                {{-- no units alert --}}
                @if(count($assets) === 0)
                    <div class="col-8 mt-4 mx-auto">
                        @include('partials.no-entry-alert', [
                            'title' => 'assets'
                        ])
                    </div>
                @endif
                {{-- end no units alert --}}

                {{-- units section --}}
                @foreach($assets as $asset)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2">
                    {{-- unit card --}}
                    <div class="card">

                        <img src="{{ $asset->image }}" alt="" class="card-img-top">

                        <div class="card-body">
                        	<h3 class="card-title">
                                {{ $asset->name }}
                            </h3>
                            <p class="card-title">
                               Control Code: {{ $asset->control_code }}
                            </p>
                            
                            <h5>Category:
                                <span class="badge badge-primary ">{{$asset->category->name}}</span>
                            </h5>
                            <h5 class="card-text mb-0">
                                Status: <span
                                            class="badge
                                                @if($asset->status_id === 1)
                                                    badge-success
                                                @elseif($asset->status_id === 2)
                                                    badge-danger
                                                @elseif($asset->status_id === 3)
                                                    badge-warning
                                                @endif
                                            ">{{ $asset->asset_status->name }}</span>
                            </h5>
                        </div>
                        <div class="card-footer bg-secondary text-white">

                            @include('assets.partials.request-btn')
                            <a href="{{ route('assets.show', $asset->id) }}" class="btn btn-sm btn-info w-100 mb-1">View</a>
                            @can('isAdmin')
                                @include('assets.partials.edit-btn')
                                @include('assets.partials.delete-form')
                            @endcan
                        </div>
                    </div>
                    {{-- unit card end --}}
                </div>
                @endforeach
                {{-- units section end --}}
            </div>
        </div>


    </div>
</div>
@endsection

