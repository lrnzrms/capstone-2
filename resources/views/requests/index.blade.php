@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">
                Request Form
            </h1>
        </div>
    </div>

    @if(!Session::has('request'))
    {{-- alert start --}}
    <div class="alert alert-info alert-dismissible fade show text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong>Your request form is empty!</strong> 
    </div>

{{-- alert end --}}
@else()
<div class="row">
    <div class="col-12">
        {{-- start cart table --}}
        <table class="table">
           <thead>
               <tr>
                   <th>Name</th>
                   <th>Control Code</th>
                   <th>Current Status</th>
                   <th>Action</th>
               </tr>
           </thead>
           <tbody>
               {{-- start item row --}}
               @foreach($assets as $asset)
                 <tr>
                  <td scope="row">{{ $asset->name }}</td>
                  <td scope="row">{{ $asset->control_code }}</td>
                  <td>
                    <span
                    class="badge
                    @if($asset->status_id === 1)
                    badge-success
                    @elseif($asset->status_id === 2)
                    badge-danger
                    @elseif($asset->status_id === 3)
                    badge-warning
                    @endif
                    ">{{ $asset->asset_status->name }}</span>
                  </td>
                  <td>
                    <form action="{{ route('requests.destroy', $asset->id) }}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn btn-sm btn-outline-danger">Remove</button>
                    </form>       
                  </td>
                </tr>
      @endforeach
      {{-- end item row --}}
  </tbody>
  
</table>
{{-- end cart table --}}
</div>
<div class="col-12 col-sm-8 mx-auto">
  <form action="{{route('tickets.store')}}" method="POST" class="mb-5">
    @csrf
    <div class="row align-items-center">
      <div class="col align-self-start">
        <div class="form-group">
          <label for="date_needed">Date needed</label>
          <input required="" type="date" name="date_needed" id="date_needed"
          class="form-control date">
          <small class="text-muted">Date when you need to have this asset</small>
        </div>
        @error('date_needed')
        <small class="d-block invalid-feedback">
          <strong>
            {{ $message }}
          </strong>
        </small>
        @enderror
      </div>
      <div class="col align-self-start">
        <div class="form-group">
          <label for="date_returned">Date to return</label>
          <input required="" type="date" name="date_returned" id="date_returned"
          class="form-control date">
          <small class="text-muted">Date when you will these assets</small>
        </div>
        @error('date_returned')
        <small class="d-block invalid-feedback">
          <strong>
            {{ $message }}
          </strong>
        </small>
        @enderror
      </div>
    </div>
    <button class="btn btn-primary w-100">Submit</button>

  </form>
</div>
</div>

{{-- clear cart --}}
<div class="row">
    <div class="col-12">
        <form action="{{ route('requests.clear') }}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-sm btn-outline-danger">Clear Cart</button>
        </form>
    </div>
</div>
@endif
</div>

@if(Route::CurrentRouteNamed('requests.index'))
<script type="text/javascript" src="{{ asset('js/date.js') }}"></script>
@endif

@endsection
