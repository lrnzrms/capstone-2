{{-- table start --}}
<div class="table-responsive">
	<table class="table table-hover">
		{{-- transacation code --}}
		<tr>
			<td>Ticket Code</td>
			<td>{{ $ticket->ticket_code }}</td>
		</tr>
		{{-- ticket code end --}}
		
		{{-- Customer name start --}}
		<tr>
			<td>User</td>
			<td>{{ $ticket->user->name }}</td>
		</tr>
		{{-- Customer name end --}}

		{{-- Date request start --}}
		<tr>
			<td>Date Request</td>
			<td>{{ date("M d, Y H:i:s", strtotime($ticket->created_at)) }}</td>
		</tr>
		{{-- Date request end --}}

		{{-- Status start --}}
		<tr>
			<td>Status</td>
				<td>
					<span class="badge badge-{{ $ticket->status->id === 1 ? "warning" : ($ticket->status->id === 2 ? "success" : "danger") }}">
						{{ $ticket->status->name }}
					</span>
					@can('isAdmin')
					@include('tickets.partials.edit-status')
					@endcan
				</td>
		</tr>

		{{-- Date Needed --}}
		<tr>
			<td>Date Needed</td>
			<td>{{ date("M d, Y", strtotime($ticket->date_needed)) }}</td>
		</tr>
        {{-- Date Needed end --}}

         {{-- Date to return --}}
		<tr>
			<td>Return Date</td>
			<td>{{ date("M d, Y", strtotime($ticket->date_returned)) }}</td>
		</tr>
		{{-- Date to return end --}}
		{{-- Status end --}}
	</table>
</div>
{{-- table end --}}