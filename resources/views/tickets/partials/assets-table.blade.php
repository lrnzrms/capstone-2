<div class="row">
	<div class="col-12">
		{{-- table start --}}
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Asset Name</th>
						<th>Control Code</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($ticket->assets as $asset)
					{{-- asset row start --}}	
					<tr>
						<td>{{ $asset->name }}</td>
						<td>{{ $asset->control_code }}</td>
						<td><span class="badge
							@if($asset->status_id === 1)
							badge-success
							@elseif($asset->status_id === 2)
							badge-danger
							@elseif($asset->status_id === 3)
							badge-warning
							@endif
							">{{ $asset->asset_status->name }}</span></td>
					</tr>	
					{{-- asset row end --}}
					@endforeach
				</tbody>
			</table>
		</div>
		{{-- table end --}}
	</div>
</div>
