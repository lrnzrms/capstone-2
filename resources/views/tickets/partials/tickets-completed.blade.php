<div class="collapse" id="completedTickets" data-parent="#accordion">
    <div class="accordion" id="accordionCompleted">

    {{-- tickets card start --}}
    @foreach($tickets as $ticket)
        @if($ticket->status_id === 4)
            @include('tickets.partials.accordion',[
                'status_name' => $ticket->status->name
            ])
        @endif
    @endforeach
    {{-- tickets card start --}}
    </div>
 </div>
