<div class="accordion" id="accordionExample">

  {{-- transaction card start --}}
  @foreach($tickets as $ticket)
    <div class="card">
      <div class="card-header" id="headingOne">
        <h2 class="mb-0">
          <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$ticket->id}}" aria-expanded="true" aria-controls="collapseOne">
            {{ $ticket->ticket_code }} | {{ date("m d, Y", strtotime($ticket->created_at)) }} |
            <span class="badge badge-{{ $ticket->status->id === 1 ? "warning" : ($ticket->status->id === 2 ? "success" : "danger") }}">
              {{ $ticket->status->name }}
            </span>
          </button>
        </h2>
      </div>

      <div id="collapse{{$ticket->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="card-body"> 
          @include('tickets.partials.summary')
          <a href="{{ route('tickets.show', $ticket->id) }}">View details</a>
        </div>
      </div>
  </div>
  {{-- transaction card start --}}
  @endforeach
</div>