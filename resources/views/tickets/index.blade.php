@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Tickets
				</h1>
			</div>
		</div>
		
		{{-- alert message --}}
        @includeWhen(Session::has('message'), 'partials.alert')

		{{-- tcikets section start --}}
		<div class="row">
			<div class="col-12">
				@include('tickets.partials.accordion')
			</div>
		</div>
		{{-- tickets section end --}}

		
	</div>
@endsection