<div class="row">
	<div class="col-12">
		<div class="alert alert-{{ Session::has('alert') ? Session::get('alert') : "info" }} text-center">
			{{ Session::get('message') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
		</div>
	</div>
</div>