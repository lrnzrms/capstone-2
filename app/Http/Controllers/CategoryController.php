<?php

namespace App\Http\Controllers;

use App\Category;
use App\Asset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->authorize('viewAny', Category::class);
        // $categories = Category::all()->sortBy('name');
        // return view('categories.index')->with('categories', $categories);

        if (Auth::check()) {
            $categories = Category::all();
            foreach ($categories as $category) {
                $assets = Asset::where('category_id', $category->id)->get();
                $availableAssets = Asset::where('status_id', 1)->where('category_id', $category->id)->get();
                $category->available_stocks = count($availableAssets);
                $category->total_stocks = count($assets);
            }
            return view('categories.index')
                ->with('categories', $categories);
                
        } else {
            return abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Category::class);
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $this->authorize('create', Category::class);
        $validatedData = $request->validate([
            'name' => 'required|unique:categories,name'
        ]);

        $category = new Category($validatedData);
        $category->save();
        return redirect(route('categories.index'))->with('message', "Category {$category->name} is added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        // $this->authorize('view', $category);
        // return view('categories.show')
        //     ->with('category', $category)
        //     ->with('assets', $assets);

        if (Auth::check()) {
            $assets = Asset::where('category_id', $category->id)->get();
            $availableAssets = Asset::where('status_id', 1)->where('category_id', $category->id)->get();
            $category->available_stocks = count($availableAssets);
            $category->total_stocks = count($assets);
            $category->save();
            return view('categories.show')
                ->with('category', $category)
                ->with('assets', $assets);
        } else {
            return abort(403, 'This action is unauthorized.');
        }
;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->authorize('update', $category);
        return view('categories.edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->authorize('update', $category);
        $validatedData = $request->validate([
            'name' => 'required|unique:categories,name'
        ]);
        $category->update($validatedData);
        $category->save();

        return redirect( route('categories.show', $category->id))->with('message', "Category is updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->authorize('delete', $category);
        $category->delete();
        return redirect(route('categories.index'))
        ->with('message', "Category {$category->name} has been deleted")
        ->with('alert', 'warning');
    }
}
