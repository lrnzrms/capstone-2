<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Category;
use App\AssetStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()) {
             $assets = Asset::all()->sortby('name');
             return view('assets.index')->with('assets', Asset::all());
        } else {
            return abort(403, 'This action is unauthorized.');
        }
           
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Asset::class);
        return view('assets.create')
            ->with('categories', Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Asset::class);

        $control_code = $request->category_id . '-' . $request->control_code;
        $request->request->add(['control_code' => $control_code]);

        $validatedData = $request->validate([
            'name' => 'required|string',
            'control_code' => 'required|string|unique:assets,control_code',
            'description' => 'required',
            'category_id' => 'required|numeric',
            'image' => 'required|image|max:2000'
        ]);

        // save the image after validation
        $path = $request->file('image')->store('public/assets');


        // this will return the url of the saved image
        $url = Storage::url($path);

        // create new product entry
        $asset = new Asset($validatedData);

        // assign the value of image 
        $asset->image = $url;

        $asset->control_code = strtoupper($asset->control_code);
        // save the product
        $asset->save();

        return redirect( route('assets.show', $asset->id))->with('message', 'Asset is added successfully.')->with('alert', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        // return view('assets.show')->with('asset', $asset);

       if (Auth::check()) {
            return view('assets.show')->with('asset', $asset);
        } else {
            return abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        $this->authorize('update', $asset);
        return view('assets.edit')
            ->with('asset', $asset)
            ->with('categories', Category::all())
            ->with('asset_statuses', AssetStatus::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        $this->authorize('update', $asset);
         $validatedData = $request->validate([
            'name' => 'required|string',
            'description' => 'required',
            'category_id' => 'required',
            'image' => 'image|max:2000',
            'status_id' => 'required|numeric'
        ]);


        // update product
        $asset->update($validatedData);

        // check if there's an image upload
        // if there's an image, save it and get the url
        if ($request->hasFile('image')) {
            // store the image
            $path = $request->file('image')->store('public/assets');
            // this will return the url of the saved image
            $url = Storage::url($path);
            // set the new url of image
            $asset->image = $url;
        }

        // save product
        $asset->save();

        return redirect( route('assets.show', $asset->id))->with('message', 'Asset is updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        $this->authorize('delete', $asset);
        $asset->delete();
        return back()
            ->with('message', "{$asset->name} has been deleted.")->with('alert', 'warning')
            ->with('categories', Category::all());
    }
}
