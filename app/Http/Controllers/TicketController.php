<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\Asset;
use App\Status;
use Illuminate\Http\Request;
use Str;
Use Auth;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $tickets = Ticket::all()->sortby('created_at');
        // $statuses = Status::all();
        // return view('tickets.index')
        //     ->with('tickets', $tickets)
        //     ->with('statuses', $statuses);

        if (Auth::check()) {
            $user_id = Auth::id();
            $statuses = Status::all();
            if ($user_id === 1) {
                //Admin
                $tickets = Ticket::all();
            } else {
                // User
                $tickets = Ticket::where('user_id', $user_id)->get();
            }
            return view('tickets.index')
                ->with('tickets', $tickets)
                ->with('statuses', $statuses);
        } else {
            return abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // // get details
        // $ticket = new Ticket;
        // // make transaction code
        // $ticket->ticket_code = strtoupper(Str::random(10));
        // // user id 
        // $ticket->user_id = Auth::user()->id;
        // // save
        // $ticket->save();

        // //populate the pivot table

        // //query to database to get the products
        // $assets = Asset::find(array_keys(session('request')));

        // foreach ($assets as $asset){
        //     $asset->quantity = session("request.$asset->id");
        //     // query to database(asset_ticket)
        //     $ticket->assets()->attach($asset->id,[
        //         'quantity' =>$asset->quantity
        //     ]);

        // }

        // $ticket->save();

        // // clear request
        // session()->forget('request'); 

       

        // return redirect( route('tickets.show', $ticket->id));

        if (Auth::check()) {
            $ticket_code = strtoupper(Str::random(10));
            $request->request->add(['ticket_code' => $ticket_code]);
            $validatedData = $request->validate([
                'ticket_code' => 'required|string|unique:tickets,ticket_code',
                'date_needed' => 'required|date_format:Y-m-d',
                'date_returned' => 'required|date_format:Y-m-d|after_or_equal:date_needed'
            ]);
            $ticket = new Ticket($validatedData);
            $ticket->user_id = Auth::user()->id;
            // $ticket->user_id =


            $ticket->save();

            $assets = Asset::find(array_keys(session('request')));

            foreach ($assets as $asset) {
                $ticket->assets()->attach($asset->id);
            }

            $ticket->save();

            session()->forget('request');

            return redirect(route('tickets.index', $ticket->id))->with('message', "New ticket was added successfully.")->with('alert', 'success');
        } else {
            return abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        $this->authorize('view', $ticket);
        $statuses = Status::all();

       return view('tickets.show')
            ->with('ticket', $ticket)
            ->with('statuses', $statuses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        // $ticket->status_id = $request->status_id;
        // $ticket->save();
        // return back();
        $this->authorize('update', $ticket);
        $ticket->status_id = $request->status_id;
        $ticket->save();

        return back()->with('message', "Ticket status was edited successfully.")->with('alert', 'info');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
