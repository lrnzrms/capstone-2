<?php

namespace App\Http\Controllers;

use App\Asset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (session()->has('request')){

        //     $assets = Asset::find(array_keys(session('request')));

        //     $total = 0;
            
        //     foreach ($assets as $asset) {
               
        //         $asset->quantity = session("request.$asset->id");
                
        //         $asset->subtotal = $asset->price * $asset->quantity;
                
        //         $total += $asset->subtotal;
        //     }

        //     return view('requests.index')
        //         ->with('assets', $assets)
        //         ->with('total', $total);
        // } else {
        //     return view('requests.index');
        // }

        if (Auth::check()) {
            if (session()->has('request')) {
                $assets = Asset::find(array_keys(session('request')));

                return view('requests.index')
                    ->with('assets', $assets);
            } else {
                return view('requests.index');
            }
        } else {
            return abort(403, 'This action is unauthorized.');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        // $request->validate([
        //     'quantity' => 'required|integer|min:1'
        // ]);
        
        // $quantity = $request->quantity;

        // $request->session()->put("request.$id", $quantity);


        // return redirect( route('requests.index'));

        if (Auth::check()) {
            $request->session()->put("request.$id");
            return redirect(route('requests.index'));
        } else {
            return abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        session()->forget("request.$id");
        if(count(session()->get('request')) === 0) {
            session()->forget('request');
        }
        return back();
    }

     public function clear()
    {
        session()->forget('request');

        return back();
    }
}
