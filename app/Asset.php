<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Asset extends Model
{
	use SoftDeletes;

	protected $fillable = ['name', 'control_code', 'description', 'image', 'category_id', 'status_id'];

	public function category()
	{
    	return $this->belongsTo('App\Category');
	}	

	public function asset_status()
    {
        return $this->belongsTo(AssetStatus::class, 'status_id');
    }

    public function tickets()
    {
        return $this->belongsToMany('App\Ticket')
            ->withTimestamps();
    }
}
