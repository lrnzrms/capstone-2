<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;

    protected $fillable = ['ticket_code', 'date_needed', 'date_returned'];

    public function status()
    {
    	return $this->belongsTo('App\Status');
    }

    public function assetStatus()
    {
    	return $this->belongsTo('App\AssetStatus');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function assets()
    {
    	return $this->belongsToMany('App\Asset')
    	 ->withTimestamps()
         ->withTrashed();;
         
    }
}
